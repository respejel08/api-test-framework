# Installation #

## Pre-requisites ##
 * [Java]
 * [Maven]
 * IDE - Eclipse / IntelliJ

## [Java](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
Version - Java 8 
### Windows
1.	Begin by locating the My Computer icon on your desktop
2.	Right click on the My Computer icon and select Properties from the menu
3.	Select the Advanced system settings from the System Properties window displayed
4.	Click on the Environment Variables button
5.	In the Environment Variables window, click the New button located within the System variables group
6.	Enter JAVA_HOME into the Variable Name: field
7.	Enter the path to your Java folder into the Variable value: field. If you're developing in Eclipse/Intellij, and using Maven, then this MUST be a path to the JDK application. Typically, this is something like C:\PROGRA~1\Java\jdk1.8.0_211
8.	Click the OK button
9.	Now, extend the PATH environment variable by adding the JAVA_HOME setting to it using the following steps:
     *	Complete Steps 1 - 4 of the How to Set Your PATH Environment
     *	Select the PATH value from the System Variables list
     *	Click the Edit button located within the Environment Variables window - the one next to the New button
     *	Append ;%JAVA_HOME%\bin to the existing value in the Variable value: field
10.	Click the OK button. This should return you back to the Environment Variables window.
11.	Click the OK button again
12.	Now, click the OK button located within the System Properties window

## [Maven](http://maven.apache.org/download.html)
1.	Download the latest version of Maven. You'll want to grab the .zip file
2.	Copy the downloaded .zip file to your C: drive
3.	Extract the .zip file to your C: drive

### Windows
1. Setup your M2_HOME environment variable by following the steps 
   * Begin by locating the My Computer icon on your desktop
   * Right click on the My Computer icon and select Properties from the menu
   * Select the Advanced system settings link from the System Properties window displayed
   * Click on the Environment Variables button
   * In the Environment Variables window, click the New button located within the System Variables group
   * Enter M2_HOME into the Variable Name: field
   * Enter the path to your Maven installation into the Variable value: field. Typically, this is something like C:\apache-maven-3.0.3\bin
   * Click the OK button. This should return you back to the Environment Variables window.
   * Click the OK button again
   * Now, click the OK button located within the System Properties window
2.	Now, extend the PATH environment variable by adding the M2_HOME setting to it using the following steps:
   * Complete Steps a-g of How to Set Your PATH Environment
   * Select the PATH value from the System Variables list
   * Click the Edit button located within the Environment Variables window - the one next to the New button
   * Append ;%M2_HOME% to the existing value in the Variable value: field
3.	Open up a console and type mvn --version at the prompt to test your installation. You should see Maven's version reported
4. Place the downloaded settings file in .M2 folder under your user folder.

## Running Instructions
1. Add Environment as a command parameter "-Denv=dev" from the console or set the property before your Request Factory class executes with System.setProperty("env", "dev");
2. Execute the RunCucumberTest class to run the test cases
