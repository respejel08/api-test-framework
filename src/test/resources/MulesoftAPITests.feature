Feature: Mulesoft API Test

  Scenario Outline: HOPK Dictionary Test
    Given Make a GET request to service: MULE_GET_HOPK_DICTIONARY with parameters:
      | type           | name     | value      |
      | PATH_PARAMETER | PIMClass | <PIMClass> |
    Then The response is successful

    Examples:
      | PIMClass            |
      | Concession          |
      | ConcessionModifier  |
      | ConcessionCombo     |
      | ConcessionItemGroup |
