Feature: PIM Core API Tests

  Scenario: Test Concessions
    Given Make a GET request to service: PIM_GET_CONCESSION with parameters:
      | type            | name         | value                                                            |
      | QUERY_PARAMETER | apiKey       | dacca686194c6d3a5129ecd33c232ec50c9534360d60edfefb47ec2a43e99709 |
      | PATH_PARAMETER  | concessionId | 27782                                                            |
      | PATH_PARAMETER  | cinemaId     | 24475                                                            |
      | PATH_PARAMETER  | channelId    | 1                                                                |
      | PATH_PARAMETER  | languageCode | es_MX                                                            |
    Then The response is successful
    And The response contains:
      | id | 27782 |

#  Scenario: Create Modifier
#    Given Make a POST request to service: PIM_CREATE_MODIFIER with parameters:
#      | type            | name        | value                                                            |
#      | QUERY_PARAMETER | apiKey      | dacca686194c6d3a5129ecd33c232ec50c9534360d60edfefb47ec2a43e99709 |
#      | JSON            | hopk        | H004534                                                          |
#      | JSON            | description | Test Modifier                                                    |
#      | JSON            | items:0     | $EMPTY                                                           |
#    Then The response is successful
#    And The response contains:
#      | id      | $EXISTS                        |
#      | success | true                           |
#      | message | Modifier created successfully. |
#    And Store modifierId as the id value from the result


#  Scenario: Update Modifier
#    Given Make a PUT request to service: PIM_UPDATE_MODIFIER with parameters:
#      | type            | name        | value                                                            |
#      | QUERY_PARAMETER | apiKey      | dacca686194c6d3a5129ecd33c232ec50c9534360d60edfefb47ec2a43e99709 |
#      | PATH_PARAMETER  | modifierId  | $modifierId                                                      |
#      | JSON            | hopk        | H004534                                                            |
#      | JSON            | description | Test Modifier Update                                             |
#    Then The response is successful
#    And The response contains:
#      | id      | $modifierId                    |
#      | success | true                           |
#      | message | Modifier updated successfully. |
