Feature: Cybersource Request Test

  Scenario: Cybersource Request
    Given Make a GET request to Cybersource service: CYBERSOURCE_TRANSACTION_DETAILS with parameters:
      | type           | name | value                  |
      | PATH_PARAMETER | id   | 5678104391736787404012 |
