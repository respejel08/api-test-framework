import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "junit:target/cucumber.xml", "json:target/cucumber.json"},
        //tags = {},
        features = {"src/test/resources/PIMCoreAPITests.feature"},
        glue = {"steps"})

public class RunCucumberTest {
}
