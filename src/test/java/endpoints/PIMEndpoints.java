package endpoints;

public final class PIMEndpoints {
    public static final String baseUrl = "https://pim";
    public static final String host = ".cinepolisdevops.com";
    public static final boolean isEnvNeeded = true;

    public enum Endpoints {
        PIM_CREATE_CONCESSION("PIM_CREATE_CONCESSION", "/v1/concessions"),
        PIM_UPDATE_CONCESSION("PIM_UPDATE_CONCESSION", "/v1/concessions/{concessionId}"),
        PIM_CREATE_COMBO("PIM_CREATE_COMBO", "/v1/combos"),
        PIM_UPDATE_COMBO("PIM_UPDATE_COMBO", "/v1/combos/{comboId}"),
        PIM_CREATE_MODIFIER_GROUP("PIM_CREATE_MODIFIER_GROUP", "/v1/concessions/modifier-groups"),
        PIM_UPDATE_MODIFIER_GROUP("PIM_UPDATE_MODIFIER_GROUP", "/v1/concessions/modifier-groups/{modifierGroupId}"),
        PIM_GET_CONCESSION("PIM_GET_CONCESSION", "/v1/concession/{concessionId}/cinema/{cinemaId}/channel/{channelId}/language/{languageCode}"),
        PIM_CREATE_MODIFIER("PIM_CREATE_MODIFIER", "/v1/concessions/modifiers"),
        PIM_UPDATE_MODIFIER("PIM_UPDATE_MODIFIER", "/v1/concessions/modifiers/{modifierId}");

        public String serviceName;
        public String path;

        Endpoints(String serviceName, String path) {
            this.path = path;
            this.serviceName = serviceName;
        }
    }
}
