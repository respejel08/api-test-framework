package endpoints;

public final class MulesoftEndpoints {
    public static final String baseUrl = "https://mul-sys-pim-sandbox";
    public static final String host = ".us-e1.cloudhub.io";
    public static final boolean isEnvNeeded = false;


    public enum Endpoints {
        MULE_GET_HOPK_DICTIONARY("MULE_GET_HOPK_DICTIONARY", "/v1/sys/concessions/dictionary/{PIMClass}/HOPK");

        public String serviceName;
        public String path;

        Endpoints(String serviceName, String path) {
            this.path = path;
            this.serviceName = serviceName;
        }
    }
}
