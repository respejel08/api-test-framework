package utils;

import static org.junit.Assert.*;

import io.cucumber.datatable.DataTable;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class JSONValidator {
    private static final String EXISTS_KEYWORD = "$EXISTS";
    private static final Logger log = LoggerFactory.getLogger(JSONValidator.class);

    public static void validateJSONResponse(DataTable data, Object response) {
        log.debug("Validating against JSON string: " + response.toString());
        Map<String, String> table = data.asMap(String.class, String.class);
        for (Map.Entry<String, String> entry : table.entrySet()) {
            validateField(response, entry.getKey(), entry.getValue());
        }
    }

    public static void validateJSONObjectDoesNotExist(DataTable data, Object response) {
        List<String> dataList = data.asList(String.class);
        for (String entry : dataList) {
            assertNull(getValue(response, entry));
        }
    }

    private static void validateField(Object rootNode, String jsonPointer, String expectedValue) {
        String actualValue = getValue(rootNode, jsonPointer);
        if (actualValue == null && expectedValue != null) {
            fail("The node " + jsonPointer + " does not exist in the JSON provided looking for a value of "
                    + expectedValue);
        } else {
            if (expectedValue != null && expectedValue.equals(EXISTS_KEYWORD)) {
                assertNotNull(jsonPointer + " does not exist in provided JSON", actualValue);
            } else {
                if (expectedValue != null && expectedValue.startsWith("$")) {
                    expectedValue = TestStateHolder.get(expectedValue.substring(1)).toString();
                }
                assertEquals("comparing value for path " + jsonPointer, expectedValue, actualValue);
            }
        }
    }

    public static String getValue(final Object rootNode, String path) {
        Object currentNode = rootNode;
        if (StringUtils.isNotBlank(path)) {
            String[] paths = path.split(":");
            for (String node : paths) {
                if (node.matches("\\d+") && currentNode instanceof JSONArray) {
                    currentNode = ((JSONArray) currentNode).get(Integer.parseInt(node));
                } else if (currentNode instanceof JSONObject) {
                    currentNode = ((JSONObject) currentNode).get(node);
                } else {
                    return null;
                }
            }
        }
        if (currentNode == null) {
            return null;
        } else if (currentNode instanceof JSONArray) {
            return ((JSONArray) currentNode).toJSONString();
        } else if (currentNode instanceof JSONObject && ((JSONObject) currentNode).isEmpty()) {
            return null;
        }
        return currentNode.toString();
    }
}
