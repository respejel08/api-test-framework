package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ContainerNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import endpoints.MulesoftEndpoints;
import endpoints.PIMEndpoints;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class RequestFactory {
    private static final Logger log = LoggerFactory.getLogger(RequestFactory.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final String EMPTY_KEYWORD = "$EMPTY";

    enum ParameterTypes {
        HEADER("HEADER"),
        PATH_PARAMETER("PATH_PARAMETER"),
        QUERY_PARAMETER("QUERY_PARAMETER"),
        JSON("JSON"),
        JSON_ARRAY("JSON_ARRAY"),
        FORM_PARAMETER("FORM_PARAMETER");

        private String type;

        ParameterTypes(String type) {
            this.type = type;
        }
    }

    private static String baseUrl;
    private static String host;
    private static String env;
    public static boolean isAuthRequired = false;
    public static String username;
    public static String password;

    public void createRequest(String method, String serviceName, List<Map<String, String>> requestParameters) throws ParseException {
        if (System.getProperty("env") == null) throw new IllegalArgumentException("Environment needs to be set");

        String servicePath = serviceNameResolver(serviceName);
        if (servicePath != null) {
            String url = baseUrl + env + host;
            log.debug("Base URL: {}", url);
            log.debug("Path: {}", servicePath);

            RequestSpecification requestSpecification;
            if (isAuthRequired) {
                //Here we will add more authentication methods if needed
                requestSpecification = RestAssured.given().auth().basic(username, password).baseUri(url).basePath(servicePath);
            } else {
                requestSpecification = RestAssured.given().baseUri(url).basePath(servicePath);
            }

            if (requestParameters != null)
                requestSpecification = mapRequestParameters(requestParameters, requestSpecification);
            Response response;

            switch (method.toUpperCase()) {
                case "GET":
                    response = RestAssured.given(requestSpecification).get();
                    break;
                case "DELETE":
                    response = RestAssured.given(requestSpecification).delete();
                    break;
                case "POST":
                    response = RestAssured.given(requestSpecification).post();
                    break;
                case "PUT":
                    response = RestAssured.given(requestSpecification).put();
                    break;
                case "PATCH":
                    response = RestAssured.given(requestSpecification).patch();
                    break;
                default:
                    throw new IllegalArgumentException("Http Method passed is not valid");
            }

            String responseBody = response.getBody().asString();

            if (!responseBody.isEmpty()) {
                JSONParser parser = new JSONParser();
                TestStateHolder.saveResponse(parser.parse(response.getBody().asString()));
            }
            TestStateHolder.saveResponseCode(response.getStatusCode());
            response.getBody().print();
            return;
        }
        throw new IllegalArgumentException("Service passed is not valid");
    }

    private String serviceNameResolver(String serviceName) {
        String endpointClass = serviceName.substring(0, serviceName.indexOf('_')).toUpperCase();
        switch (endpointClass) {
            case "PIM":
                baseUrl = PIMEndpoints.baseUrl;
                host = PIMEndpoints.host;
                env = PIMEndpoints.isEnvNeeded ? System.getProperty("env") : "";
                for (PIMEndpoints.Endpoints endpoint : PIMEndpoints.Endpoints.values()) {
                    if (endpoint.serviceName.equals(serviceName)) return endpoint.path;
                }
                break;
            case "MULE":
                baseUrl = MulesoftEndpoints.baseUrl;
                host = MulesoftEndpoints.host;
                env = MulesoftEndpoints.isEnvNeeded ? System.getProperty("env") : "";
                for (MulesoftEndpoints.Endpoints endpoint : MulesoftEndpoints.Endpoints.values()) {
                    if (endpoint.serviceName.equals(serviceName)) return endpoint.path;
                }
                break;
            default:
                return null;
        }
        return null;
    }

    private RequestSpecification mapRequestParameters(List<Map<String, String>> requestParameters, RequestSpecification requestSpecification) {
        boolean isJSON = false;
        boolean isJSONArray = false;
        Map<String, String> queryParamsMap = new HashMap<>();
        Map<String, String> headersMap = new HashMap<>();
        Map<String, String> bodyMap = new LinkedHashMap<>();
        Map<String, String> pathParamsMap = new HashMap<>();
        Map<String, String> formParamsMap = new HashMap<>();

        for (Map<String, String> requestParameter : requestParameters) {
            String value = resolveValue(requestParameter.get("value"));

            if (requestParameter.get("type").toUpperCase().equals(ParameterTypes.HEADER.type)) {
                headersMap.put(requestParameter.get("name"), value);
            } else if (requestParameter.get("type").toUpperCase().equals(ParameterTypes.QUERY_PARAMETER.type)) {
                queryParamsMap.put(requestParameter.get("name"), value);
            } else if (requestParameter.get("type").toUpperCase().equals(ParameterTypes.JSON.type)) {
                isJSON = true;
                bodyMap.put(requestParameter.get("name"), value);
            } else if (requestParameter.get("type").toUpperCase().equals(ParameterTypes.JSON_ARRAY.type)) {
                isJSONArray = true;
                bodyMap.put(requestParameter.get("name"), value);
            } else if (requestParameter.get("type").toUpperCase().equals(ParameterTypes.PATH_PARAMETER.type)) {
                pathParamsMap.put(requestParameter.get("name"), value);
            } else if (requestParameter.get("type").toUpperCase().equals(ParameterTypes.FORM_PARAMETER.type)) {
                formParamsMap.put(requestParameter.get("name"), value);
            }
        }

        if (isJSON && isJSONArray) throw new IllegalArgumentException("Request doesn't accept combining JSON and" +
                " JSON_ARRAY parameters, if your request body starts with a nameless JSON array made all the parameter " +
                "JSON_ARRAY otherwise use JSON");
        if (!headersMap.isEmpty()) requestSpecification =
                buildRequestSpecification(requestSpecification, headersMap, ParameterTypes.HEADER);
        if (!queryParamsMap.isEmpty()) requestSpecification =
                buildRequestSpecification(requestSpecification, queryParamsMap, ParameterTypes.QUERY_PARAMETER);
        if (!pathParamsMap.isEmpty()) requestSpecification =
                buildRequestSpecification(requestSpecification, pathParamsMap, ParameterTypes.PATH_PARAMETER);
        if (!formParamsMap.isEmpty()) requestSpecification =
                buildRequestSpecification(requestSpecification, formParamsMap, ParameterTypes.FORM_PARAMETER);
        if (!bodyMap.isEmpty()) {
            if (isJSON) {
                requestSpecification =
                        buildRequestSpecification(requestSpecification, formParamsMap, ParameterTypes.JSON);
            } else if (isJSONArray) {
                requestSpecification =
                        buildRequestSpecification(requestSpecification, formParamsMap, ParameterTypes.JSON_ARRAY);
            }
        }
        return requestSpecification;
    }

    private RequestSpecification buildRequestSpecification(RequestSpecification requestSpecification, Map<String, String> paramMap, ParameterTypes type) {
        switch (type) {
            case HEADER:
                return requestSpecification.headers(paramMap);
            case FORM_PARAMETER:
                return requestSpecification.formParams(paramMap);
            case PATH_PARAMETER:
                return requestSpecification.pathParams(paramMap);
            case QUERY_PARAMETER:
                return requestSpecification.queryParams(paramMap);
            case JSON_ARRAY:
                return requestSpecification.body(getJsonArray(paramMap));
            case JSON:
                ObjectNode rootNode = objectMapper.createObjectNode();
                getJson(paramMap, rootNode, new LinkedHashMap<>());
                return requestSpecification.body(rootNode.toString());
            default:
                return requestSpecification;
        }
    }

    private String resolveValue(String value) {
        if (value.equals(EMPTY_KEYWORD)) {
            return "";
        } else if (value.startsWith("$")) {
            return TestStateHolder.get(value.substring(1)).toString();
        } else if (value.startsWith("\"$") && value.endsWith("\"")) {
            value = value.replace("\"", "");
            return "\"" + TestStateHolder.get(value.substring(1)).toString() + "\"";
        } else if (value.startsWith("\\$")) {
            return value.replace("\\$", "$");
        } else {
            return value;
        }
    }

    private static void getJson(Map<String, String> values, ObjectNode rootNode,
                                Map<String, ContainerNode<?>> jsonObjects) {

        for (Map.Entry<String, String> entry : values.entrySet()) {
            String nodePath = entry.getKey();
            String nodeValue = entry.getValue();
            if (nodePath.contains(":")) {
                String nodeParent =
                        nodePath.substring(0, nodePath.lastIndexOf(":")).replace(StringUtils.SPACE, StringUtils.EMPTY);
                String nodeName = nodePath.substring(nodePath.lastIndexOf(":") + 1);
                createNodes(rootNode, nodeParent, nodeName, jsonObjects);
                setField(jsonObjects.get(nodeParent), nodeName, nodeValue);
            } else {
                setField(rootNode, nodePath, nodeValue);
            }
        }
    }

    private static String getJsonArray(Map<String, String> values) {
        ArrayNode arrayNode = objectMapper.createArrayNode();
        Map<String, ContainerNode<?>> jsonObjects = new HashMap<>();
        for (Map.Entry<String, String> entry : values.entrySet()) {
            String nodePath = entry.getKey();
            String arrayIndex = nodePath.substring(0, nodePath.indexOf(":"));
            if (!jsonObjects.containsKey(arrayIndex)) {
                jsonObjects.put(arrayIndex, arrayNode.addObject());
            }
        }
        getJson(values, null, jsonObjects);
        return arrayNode.toString();
    }

    private static void createNodes(ObjectNode rootNode, String nodeParent, String nodeName,
                                    Map<String, ContainerNode<?>> jsonObjects) {
        String[] parentParts = nodeParent.split(":");
        String previousParent = null;
        // if the node name is simply digits, then this is an array of
        // primitives
        boolean isPrimitiveArray = nodeName.matches("\\d+");
        for (int i = 0; i < parentParts.length; i++) {
            String parentNodeName = parentParts[i].trim();
            String parentNodePath = parentParts[i].trim();

            if (previousParent != null) {
                parentNodePath = previousParent + ":" + parentNodeName;
            }
            boolean isArray = false;
            if (i < parentParts.length - 1) {
                String nextNode = parentParts[i + 1].trim();
                if (nextNode.matches("\\d+")) {
                    parentNodePath = parentNodePath + ":" + nextNode;
                    isArray = true;
                    i++;
                }
            }
            if (!jsonObjects.containsKey(parentNodePath)) {
                // no parents, create one out of the top-most parent
                if (jsonObjects.containsKey(previousParent)) {
                    createJsonObject((ObjectNode) jsonObjects.get(previousParent), isArray, isPrimitiveArray,
                            parentNodePath, parentNodeName, jsonObjects);
                } else {
                    createJsonObject(rootNode, isArray, isPrimitiveArray, parentNodePath, parentNodeName, jsonObjects);
                }
            }
            previousParent = parentNodePath;
        }
    }

    private static void createJsonObject(ObjectNode previousParent, boolean isArray, boolean isPrimitiveArray,
                                         String parentNodePath, String parentNodeName, Map<String, ContainerNode<?>> jsonObjects) {
        if (isArray || isPrimitiveArray) {
            ArrayNode parentNode = previousParent.withArray(parentNodeName);
            if (isPrimitiveArray) {
                jsonObjects.put(parentNodePath, parentNode);
            } else {
                ObjectNode firstNode = parentNode.addObject();
                jsonObjects.put(parentNodePath, firstNode);
            }
        } else {
            ObjectNode parentNode = previousParent.with(parentNodeName);
            jsonObjects.put(parentNodePath, parentNode);
        }
    }

    private static void setField(ContainerNode<?> node, String nodeName, String nodeValue) {
        Object value = convert(nodeValue);
        if (node instanceof ArrayNode && nodeValue.isEmpty()) return;
        if (value == null) {
            ((ObjectNode) node).put(nodeName, (String) null);
        } else if (value instanceof Integer) {
            if (node instanceof ObjectNode) {
                ((ObjectNode) node).put(nodeName, (Integer) value);
            } else {
                ((ArrayNode) node).add((Integer) value);
            }
        } else if (value instanceof Double) {
            if (node instanceof ObjectNode) {
                ((ObjectNode) node).put(nodeName, (Double) value);
            } else {
                ((ArrayNode) node).add((Double) value);
            }
        } else if (value instanceof Boolean) {
            if (node instanceof ObjectNode) {
                ((ObjectNode) node).put(nodeName, (Boolean) value);
            } else {
                ((ArrayNode) node).add((Boolean) value);
            }
        } else if (value instanceof String) {
            // strip the beginning and ending double quotes if this string was wrapped in them.
            // This provides a way to specify a number value wrapped in double quotes ("92119")
            // and have it treated as a json string, instead of the default int.
            // if value was a String, then it's the same as nodeValue
            if (nodeValue.startsWith("\"") && nodeValue.endsWith("\"")) {
                nodeValue = nodeValue.substring(1, nodeValue.length() - 1);
            }
            if (node instanceof ObjectNode) {
                ((ObjectNode) node).put(nodeName, nodeValue);
            } else {
                ((ArrayNode) node).add(nodeValue);
            }
        }
    }

    private static Object convert(String value) {
        if (StringUtils.isNotBlank(value)) {
            if (value.startsWith("\"") && value.endsWith("\"")) {
                value = value.replace("\"", "");
                return value;
            }
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e1) {
                try {
                    return Double.parseDouble(value);
                } catch (NumberFormatException e2) {
                    if (value.equalsIgnoreCase(Boolean.TRUE.toString())
                            || value.equalsIgnoreCase(Boolean.FALSE.toString())) {
                        return Boolean.parseBoolean(value);
                    }
                }
            }
        }
        return value;
    }
}
