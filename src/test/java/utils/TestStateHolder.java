package utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class TestStateHolder {
    private static final Logger log = LoggerFactory.getLogger(TestStateHolder.class);

    public static final String LATEST_RESPONSE = "LATEST_RESPONSE";
    public static final String LATEST_RESPONSE_CODE = "LATEST_RESPONSE_CODE";
    private static final String TODAY = "TODAY";
    private static Map<String, Object> stateHolderMap;

    /**
     * Stores {@code value} in {@link TestStateHolder} with {@code key} as key.
     *
     * @param key   String key value
     * @param value {@link Object} value to store with {@code key} as key.
     */
    public static void put(String key, Object value) {
        log.debug("Putting [{}]=[{}]", key, value);

        if (key.equals(LATEST_RESPONSE) || key.equals(LATEST_RESPONSE_CODE) || key.equals(TODAY)) {
            throw new IllegalArgumentException("LATEST_RESPONSE, LATEST_RESPONSE_CODE and TODAY are a reserved keys, please use another key");
        } else {
            if (stateHolderMap == null) {
                stateHolderMap = new HashMap<>();
            }
            stateHolderMap.put(key, value);
        }
    }

    /**
     * Retrieves previously stored value for {@code key} as key.
     *
     * @param key String key value
     * @return {@link Object} stored value with {@code key} as key.
     */
    public static Object get(String key) {
        log.debug("Getting [{}]", key);
        if (key.contains(TODAY)) return StringInterpolation.interpolate(key);
        if (stateHolderMap.containsKey(key)) {
            return stateHolderMap.get(key);
        }
        log.error("ERROR: There is no existing information for key {}", key);
        throw new IllegalArgumentException("Invalid key " + key);
    }

    /**
     * Removes {@code key} from {@link TestStateHolder}
     *
     * @param key String key value
     */
    public static void remove(String key) {
        log.debug("Removing [{}]", key);
        if (StringUtils.isBlank(key)) {
            throw new IllegalArgumentException("Key cannot be null or empty");
        }
        stateHolderMap.remove(key);
    }

    static void saveResponse(Object response) {
        if (stateHolderMap == null) {
            stateHolderMap = new HashMap<>();
        }
        stateHolderMap.put(LATEST_RESPONSE, response);
    }

    static void saveResponseCode(int code) {
        if (stateHolderMap == null) {
            stateHolderMap = new HashMap<>();
        }
        stateHolderMap.put(LATEST_RESPONSE_CODE, code);
    }
}
