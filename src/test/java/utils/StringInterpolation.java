package utils;

import me.xdrop.jrand.JRand;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;

final class StringInterpolation {
    private static final String ADD = "ADD";
    private static final String SUBTRACT = "SUBTRACT";

    static String interpolate(String placeholder) {
        if (placeholder.isEmpty()) throw new IllegalArgumentException("Empty string to interpolate");

        if (placeholder.toUpperCase().contains("TODAY")) {
            return todayInterpolator(placeholder);
        } else if (placeholder.toUpperCase().contains("RAND_INT") || placeholder.toUpperCase().contains("RAND_DBL")) {
            return randomNumberInterpolator(placeholder);
        }
        throw new IllegalArgumentException("The name of the string to interpolate does not exist");
    }

    private static String todayInterpolator(String placeholder) {
        String format = placeholder.substring(placeholder.indexOf("(") + 1, placeholder.indexOf(")"));
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Calendar today = Calendar.getInstance();
        String operation;

        String offsetString;
        if (placeholder.contains("+")) {
            offsetString = placeholder.substring(placeholder.indexOf("+") + 1).trim();
            operation = ADD;
        } else if (placeholder.contains("-")) {
            offsetString = placeholder.substring(placeholder.indexOf("-") + 1).trim();
            operation = SUBTRACT;
        } else {
            return dateFormat.format(today.getTime());
        }

        int offset = Integer.parseInt(offsetString.substring(0, offsetString.indexOf(" ")));
        switch (offsetString.substring(offsetString.indexOf(" ") + 1).trim().toUpperCase()) {
            case "DAY":
            case "DAYS":
                if (operation.equals(ADD)) {
                    today.add(Calendar.DATE, offset);
                } else {
                    today.add(Calendar.DATE, -offset);
                }
                break;
            case "MONTH":
            case "MONTHS":
                if (operation.equals(ADD)) {
                    today.add(Calendar.MONTH, offset);
                } else {
                    today.add(Calendar.MONTH, -offset);
                }
                break;
            case "YEAR":
            case "YEARS":
                if (operation.equals(ADD)) {
                    today.add(Calendar.YEAR, offset);
                } else {
                    today.add(Calendar.YEAR, -offset);
                }
                break;
        }
        return dateFormat.format(today.getTime());
    }

    /**
     * Replaces the provided {@code placeholder} for a {@link String} with a random number
     * Possible combinations are:
     * - RAND_INT: which returns a {@link String} of a random {@link Integer} number.
     * - RAND_INT(2, 3): range values need to be {@link Integer} and returns a {@link String} of a random {@link Integer}
     * number between the provided range.
     * - RAND_DBL: which returns a {@link String} of a random {@link Double} number.
     * - RAND_DBL(3): number of decimals must be an integer and returns a {@link String} of a random {@link Double}
     * number with the specified decimal number.
     * - RAND_DBL(2, 5.5) range values need to be {@link Double} and returns a {@link String} of a random {@link Double}
     * number between the provided range.
     * - RAND_DBL(3.4, 6.95. 2): range values need to be {@link Double} and number of decimals must be an integer and
     * returns a {@link String} of a random {@link Double} number between the provided range with the specified
     * decimal number.
     *
     * @param placeholder {@link String}
     * @return {@link String}
     **/
    private static String randomNumberInterpolator(String placeholder) {
        String pattern = "^((RAND_INT|RAND_INT\\(\\d+,\\d+\\))|(RAND_DBL|RAND_DBL(\\(\\d+\\)|\\((\\d+|\\d+.\\d+),(\\d+|\\d+.\\d+)\\)|\\((\\d+|\\d+.\\d+),(\\d+|\\d+.\\d+),\\d+\\))))$";
        if (!Pattern.matches(pattern, placeholder))
            throw new IllegalArgumentException("The RAND_INT or RAD_DBL string to interpolate is malformed");

        String numberType;
        String parameters;
        String[] params;
        if (!placeholder.contains("(")) {
            numberType = placeholder;
            parameters = null;
            params = null;
        } else {
            numberType = placeholder.substring(0, placeholder.indexOf("(")).toUpperCase();
            parameters = placeholder.substring(placeholder.indexOf("(") + 1, placeholder.indexOf(")"));
            params = parameters.split(",");
        }

        if (numberType.equals("RAND_INT")) {
            if (parameters == null) return "" + JRand.natural().random().randInt();
            return JRand.natural().range(Integer.parseInt(params[0]), Integer.parseInt(params[1])).genString();
        } else {
            if (parameters == null) return "" + JRand.natural().random().randDouble();
            if (params.length == 1) {
                String format = "%." + params[0] + "f";
                return String.format(format,
                        JRand.natural().random().randDouble());
            } else if (params.length == 2) {
                return JRand.dbl().range(Double.parseDouble(params[0]), Double.parseDouble(params[1])).genString();
            } else {
                String format = "%." + params[2] + "f";
                return String.format(format,
                        JRand.dbl().range(Double.parseDouble(params[0]), Double.parseDouble(params[1])).gen());
            }
        }
    }
}
