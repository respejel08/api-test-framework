package utils;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public final class CybersourceRequestHeaders {
    private static Properties properties;
    private static String endpoint;
    private List<Map<String, String>> requestParams;
    private static final String[] requiredHeaders = {"keyid", "host", "date", "", "v-c-merchant-id", "signature"};

    public CybersourceRequestHeaders(List<Map<String, String>> requestParams) throws IOException {
        this.requestParams = requestParams;
        properties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream("/cybersource.properties");
        properties.load(stream);
    }

    public List<Map<String, String>> buildCybersourceHeaders() throws NoSuchAlgorithmException, InvalidKeyException {
        String date = DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.now(ZoneId.of("GMT")));
        String requestTarget = "";
        for (String header : requiredHeaders) {
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put("type", "HEADER");
            headerMap.put("name", header);

            switch (header) {
                case "keyid":
                    headerMap.put("value", properties.getProperty("keyId"));
                    break;
                case "host":
                    headerMap.put("value", properties.getProperty("host"));
                    break;
                case "date":
                    headerMap.put("value", date);
                    break;
                case "v-c-merchant-id":
                    headerMap.put("value", properties.getProperty("merchantId"));
                    break;
                case "(request-target)":
                    //TODO
                    requestTarget = "abc";
                    break;
                case "signature":
                    if (requestTarget.isEmpty()) {
                        throw new IllegalArgumentException("(request-target) header needs to be generated before signature header");
                    }
                    String signatureParams = "host: " + properties.getProperty("host") + "\n" +
                            "date: " + date + "\n" +
                            "(request-target): " + requestTarget + "\n" +
                            "v-c-merchant-id: " + properties.getProperty("merchantId");

                    String signature = generateSignatureFromParams(properties.getProperty("sharedSecretKey"), signatureParams);
                    String signatureHeader = "keyid=\"" + properties.getProperty("keyId") +
                            "\", algorithm=\"HmacSHA256\", headers=\"host date (request-target) v-c-merchant-id\", signature=\"" +
                            signature + "\"";
                    headerMap.put("value", signatureHeader);
                    break;
                default:
                    break;
            }
            requestParams.add(headerMap);
        }
        return requestParams;
    }

    private String generateSignatureFromParams(String keyString, String signatureParams) throws InvalidKeyException, NoSuchAlgorithmException {
        byte[] decodedKey = Base64.getDecoder().decode(keyString);
        SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");
        Mac hmacSha256 = Mac.getInstance("HmacSHA256");
        hmacSha256.init(originalKey);
        hmacSha256.update(signatureParams.getBytes());
        byte[] HmachSha256DigestBytes = hmacSha256.doFinal();
        return Base64.getEncoder().encodeToString(HmachSha256DigestBytes);
    }
}
