package steps;

import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.json.simple.parser.ParseException;
import utils.CybersourceRequestHeaders;
import utils.RequestFactory;
import utils.TestStateHolder;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

public class RequestSteps {
    private static RequestFactory requestFactory;

    @When("^Make a (GET|POST|PUT|PATCH|DELETE) request to service: ([A-z0-9_]+) with parameters:$")
    public void makeRequestToService(String method, String service, DataTable requestParametersTable) throws ParseException {
        if (requestFactory == null) requestFactory = new RequestFactory();
        List<Map<String, String>> requestParameters = requestParametersTable.asMaps(String.class, String.class);
        RequestFactory.isAuthRequired = false;
        requestFactory.createRequest(method, service, requestParameters);
    }

    @When("^Make a (GET|POST|PUT|PATCH|DELETE) request to service: ([A-z0-9_]+) without parameters$")
    public void makeRequestToService(String method, String service) throws ParseException {
        if (requestFactory == null) requestFactory = new RequestFactory();
        RequestFactory.isAuthRequired = false;
        requestFactory.createRequest(method, service, null);
    }

    @When("^Make a (GET|POST|PUT|PATCH|DELETE) request to service: ([A-z0-9_]+) with username: (.+) and password: (.+) with parameters:$")
    public void makeRequestToServiceBasicAuth(String method, String service, String username, String password, DataTable requestParametersTable) throws ParseException {
        if (requestFactory == null) requestFactory = new RequestFactory();
        List<Map<String, String>> requestParameters = requestParametersTable.asMaps(String.class, String.class);
        RequestFactory.username = username;
        RequestFactory.password = password;
        RequestFactory.isAuthRequired = true;
        requestFactory.createRequest(method, service, requestParameters);
    }

    @When("^Make a (GET|POST|PUT|PATCH|DELETE) request to service: ([A-z0-9_]+) with username: (.+) and password: (.+) without parameters$")
    public void makeRequestToServiceBasicAuth(String method, String service, String username, String password) throws ParseException {
        if (requestFactory == null) requestFactory = new RequestFactory();
        RequestFactory.username = username;
        RequestFactory.password = password;
        RequestFactory.isAuthRequired = true;
        requestFactory.createRequest(method, service, null);
    }

    @When("^Make a (GET|POST|PUT|PATCH|DELETE) request to Cybersource service: ([A-z0-9_]+) with parameters:$")
    public void makeRequestToCybersourceService(String method, String service, DataTable requestParametersTable)
            throws ParseException, IOException, InvalidKeyException, NoSuchAlgorithmException {
        List<Map<String, String>> initRequestParameters = requestParametersTable.asMaps(String.class, String.class);
        CybersourceRequestHeaders cybersourceRequestHeaders = new CybersourceRequestHeaders(initRequestParameters);
        List<Map<String, String>> finalRequestParameters = cybersourceRequestHeaders.buildCybersourceHeaders();
        if (requestFactory == null) requestFactory = new RequestFactory();
        RequestFactory.isAuthRequired = false;
        requestFactory.createRequest(method, service, finalRequestParameters);
    }

    @When("^Store (.+) as (.+) on the state holder$")
    public void store_value_as_key_on_map(Object value, String key) {
        TestStateHolder.put(key, value);
    }
}
