package steps;

import static org.junit.Assert.*;

import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.JSONValidator;
import utils.TestStateHolder;

import java.util.regex.Pattern;

public class ResponseValidationSteps {
    private static final Logger log = LoggerFactory.getLogger(ResponseValidationSteps.class);

    @Then("^The response code should be (\\d+)")
    public void the_response_code_should_be(int expectedHttpResponseCode) {
        int code = (int) TestStateHolder.get(TestStateHolder.LATEST_RESPONSE_CODE);
        log.debug("Expected response code: {}, Actual response code: {}", expectedHttpResponseCode, code);
        assertEquals(expectedHttpResponseCode, code);
    }

    @Then("^The response is successful")
    public void the_response_is_successful() {
        String code = TestStateHolder.get(TestStateHolder.LATEST_RESPONSE_CODE).toString();
        log.debug("Response code: {}", code);
        assertTrue(Pattern.matches("2\\d{2}", code));
    }

    @Then("^The response is failure")
    public void the_response_is_failure() {
        String code = TestStateHolder.get(TestStateHolder.LATEST_RESPONSE_CODE).toString();
        log.debug("Response code: {}", code);
        assertTrue(Pattern.matches("[4,5]\\d{2}", code));
    }

    @Then("^The response contains:$")
    public void the_response_contains(DataTable data) {
        JSONValidator.validateJSONResponse(data, TestStateHolder.get(TestStateHolder.LATEST_RESPONSE));
    }

    @Then("^The response does not contain:$")
    public void the_response_does_not_contain(DataTable data) {
        JSONValidator.validateJSONObjectDoesNotExist(data, TestStateHolder.get(TestStateHolder.LATEST_RESPONSE));
    }

    @Then("^Store (.+) as the (.+) value from the result")
    public void store_x_as_the_x_value_from_the_result(String key, String fieldPointer) {
        Object objectToValidate = TestStateHolder.get(TestStateHolder.LATEST_RESPONSE);
        TestStateHolder.put(key, JSONValidator.getValue(objectToValidate, fieldPointer));
    }
}
